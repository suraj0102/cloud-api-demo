const express = require("express");
require("dotenv").config();
const axios = require("axios");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

//send message
app.post("/message", (req, res) => {
  //   const data = JSON.stringify(req.body);
  const data = req.body;
  var config = {
    method: "post",
    url: `https://graph.facebook.com/v15.0/${process.env.MOBILEID}/messages`,
    headers: {
      Authorization: `Bearer ${process.env.TOKEN}`,
      "Content-Type": "application/json",
    },
    data: data,
  };

  axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      res.send(response.data);
    })
    .catch(function (error) {
      //   console.log(error);
      res.status(400).send(error);
    });
});

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Server is running at port: ${port}`);
});
